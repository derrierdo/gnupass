import shlib

def GNUgetpass(user):
  return shlib.Run(f'pass show {user}',modes='OW').stdout.strip()


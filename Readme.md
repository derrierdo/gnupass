# Install

```
pip install git+https://gitlab.com/derrierdo/gnupass.git
```

# Usage


```
import GNUpass
pp=GNUpass.GNUgetpass('password/user')
# Password will be asked

```



